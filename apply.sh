#!/bin/bash
# Экспорт переименных в гитлаб
export ID_PROJECT_GITLAB=change me
export GLOBAL_TOKEN_GITLAB=change me
export TOKEN_RUNNER=change me
envsubst '$TOKEN_RUNNER' < helm-chart/values-gitlabrunner.yaml.dist > helm-chart/values-gitlabrunner.yaml
cd terraform
terraform init
terraform apply --auto-approve
cd ..
cp ansible/templates/control1/root/.kube/config ~/.kube/config
export KUBE_CONFIG=$(cat ansible/templates/control1/root/.kube/config | base64)
curl --request PUT --header "PRIVATE-TOKEN: $GLOBAL_TOKEN_GITLAB" \
     "https://gitlab.com/api/v4/projects/$ID_PROJECT_GITLAB/variables/KUBE_CONFIG" --form "value=$KUBE_CONFIG"
helm upgrade --install --create-namespace --values helm-chart/values-nginx-controller.yaml ingress-nginx ingress-nginx -n ingress \
--repo https://kubernetes.github.io/ingress-nginx \
--set controller.metrics.enabled=true \
--set-string controller.podAnnotations."prometheus\.io/scrape"="true" \
--set-string controller.podAnnotations."prometheus\.io/port"="10254"
helm upgrade --install --create-namespace --values helm-chart/values-gitlabrunner.yaml gitlab-runner -n gitlab gitlab/gitlab-runner





