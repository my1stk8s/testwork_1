# Variables

variable "token" {
  type = string
  description = "Yandex Cloud API key"
}

variable "cloud_id" {
  type = string
  description = "Yandex Cloud id"
}

variable "folder_id" {
  type = string
  description = "Yandex Cloud folder id"
}

variable "ssh_key_private" {
  description = "ssh"
  type        = string
  default     = "~/.ssh/toster2"
}
variable "dns_record" {
  description = "dns"
  type        = string
  default     = "moskvitin.website."
}