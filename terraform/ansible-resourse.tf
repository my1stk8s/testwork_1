resource "local_file" "inventory" {
  filename = "../ansible/host"
  content = <<EOF
[control_plane]
control1 ansible_host=${yandex_compute_instance_group.k8s-masters.instances[0].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}
control2 ansible_host=${yandex_compute_instance_group.k8s-masters.instances[1].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}
control3 ansible_host=${yandex_compute_instance_group.k8s-masters.instances[2].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}

[workers]
worker1 ansible_host=${yandex_compute_instance_group.k8s-workers.instances[0].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}

[lb]
lb1 ansible_host=${yandex_compute_instance_group.k8s-haproxy.instances[0].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}

[all:vars]
host_key_checking=False
ansible_python_interpreter=/usr/bin/python3

EOF
depends_on = [
    yandex_compute_instance_group.k8s-masters
  ]
}
resource "local_file" "inventory2" {
  filename = "../ansible/templates/join_master.sh"
  content = <<EOF
#!/bin/bash
kubeadm init --control-plane-endpoint ${yandex_compute_instance_group.k8s-masters.instances[0].network_interface[0].nat_ip_address} --upload-certs --pod-network-cidr 10.244.0.0/16  | tee -a /home/ubuntu/kubeadm.log
EOF
depends_on = [
    yandex_compute_instance_group.k8s-masters
  ]
}

resource "null_resource" "ConfigureAnsible1" {
  provisioner "local-exec" {
    command = "sleep 60; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../ansible/host ../ansible/kube-ansible.yml"
  }
  depends_on = [
    local_file.inventory
  ]
}
resource "null_resource" "ConfigureAnsible2" {
  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../ansible/host ../ansible/join-all-nodes.yml"
  }
  depends_on = [
    null_resource.ConfigureAnsible1
  ]
}

