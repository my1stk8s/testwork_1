# создание зоны DNS и определение имен доменов высшего уровня 
resource "yandex_dns_zone" "moskvitin-k8s" {
  name        = "moskvitin-zone-k8s"
  description = "dns"
  zone    = "${var.dns_record}"
  public  = true
  private_networks = [yandex_vpc_network.k8s-network.id]
}
resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.moskvitin-k8s.id
  name    = "s.${var.dns_record}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance_group.k8s-haproxy.instances[0].network_interface[0].nat_ip_address]
}
