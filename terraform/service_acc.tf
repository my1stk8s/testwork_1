# service acc create
resource "yandex_iam_service_account" "ig" {
  name        = "ig"
  description = "service account to manage IG"
  folder_id   = var.folder_id
}
resource "yandex_resourcemanager_folder_iam_member" "editor" {
  folder_id = var.folder_id
  role      = "editor"
  member   = "serviceAccount:${yandex_iam_service_account.ig.id}"
  depends_on = [yandex_iam_service_account.ig]
}

