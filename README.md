# YandexCloudKubernetes-TestWork

Тестовое задание :

- Создание Kubernetes кластера  в облаке [YandexCloud](https://https://cloud.yandex.ru) с помощью инструмента `Terraform`;
- Cам кластер развернут и настроен  с помощью [Ansible] и утилиты [Kubeadm]
- Конфигурация кластера: установка [Nginx-Ingress-Controller], [Gitlab-Runner]
- Настроен деплой в кластер символического приложения [Nginx] в разные неймспейсы согласно тех заданию используя Gitlab CI/CD а так же деплой манифестов [NetworkPolicy] и [Ingress]

## Требования для запуска проекта:

1. Зарегистрируйте аккаунт [YandexCloud](https://https://cloud.yandex.ru)
2. Установите [YandexCloud CLI](https://cloud.yandex.ru/docs/cli/operations/install-cli) для управления аккаунтом и ресурсами
3. Далее необходимо получить [AuthToken](https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token)
4. [Cконфигурируйте YandexCloud CLI](https://cloud.yandex.ru/docs/cli/operations/authentication/user), используя раннее полученный `AuthToken`
5. В Папке проетка по пути /terraform подставьте значения в файл private.auto.tfvars.dist и переименуйте его private.auto.tfvars
6. Купите домен у любого регистратора или зарегистрировать бесплатный, получить сертификат [Let's Encrypt](https://cloud.yandex.com/en/docs/certificate-manager/concepts/managed-certificate) и [прописать NS-сервера у регистратора домена](https://cloud.yandex.ru/docs/storage/operations/hosting/own-domain)
7. Установите [Terraform](https://www.terraform.io/downloads)
8. В Папке проетка в файле apply.sh подставьте значения TOKEN_RUNNER: для регистрации Gitlabrunner для версии >= 16
9. В Папке проетка в файле apply.sh подставьте значения  ID_PROJECT_GITLAB соответствующую вашему ID репозиторию  на Gitlab
10. В Папке проетка в файле apply.sh подставьте значения  GLOBAL_TOKEN_GITLAB соответствующую вашему Глобальному токену  на Gitlab

## Описание инфраструктуры проекта:

Инфраструктура проекта предусматривает создание следующих элементов:

| Элемент | Назначение |
| --- | ----------- |
| [Yandex_kubernetes_cluster] | Kubernetes кластер. Состоит из группы 3ех мастер нод группа [Haproxy] в качестве лоадбалансера и группа 1 воркер ноды.
| [Yandex_vpc_network] | Необходимо для создания изолированной сетевой инфраструктуры |
| [Yandex_vpc_subnet] | Необходимы для разделения сетевых ресурсов на публичные и приватные. В данном случае создаются три группы подсетей в трех зонах доступности|

> Таблица 1.

Также в кластере с помощью `Helm charts` устанавливаются и конфигурируются следующие компоненты:

| Компонент | Назначение |
| --- | ----------- |
| [Nginx-Ingress-Controller](https://cloud.yandex.ru/docs/managed-kubernetes/tutorials/ingress-cert-manager) | Необходим для создания эндпоинтов на сервисах `Kubernetes` типа `Nodeport` |
| [Gitlab-Runner](https://gitlab.com/gitlab-org/charts/gitlab-runner) | Служит для работы с `Gitlab CI/CD`.| 


> Таблица 2.

1. Склонируйте репозиторий к себе для работы с ним:
   ```shell
   git clone https://gitlab.com/my1stk8s/testwork_1.git
   ```
> Таблица 3.

2. Инициализируйте Terraform и запустите создание ресурсов через баш скрипт в кореной дериктории с проектом:

   ```shell
    ./apply.sh
   ```
3. Для удаления всех рессурсов 
   ```shell
    ./destroy.sh
   ```
